#!/bin/bash
# exit when any command fails
set -e

echo "Pulling submodules: git submodule init"
git submodule init
echo "Pulling submodules: git submodule update"
git submodule update

echo "Copy host key file: cp -v ~/.ssh/id_rsa thomaswoltjer.com/host_key"
cp -v ~/.ssh/id_rsa thomaswoltjer.com/host_key
echo "Copy host key file: cp -v ~/.ssh/id_rsa firstrpc.org/host_key"
cp -v ~/.ssh/id_rsa firstrpc.org/host_key



echo "Setting up thomaswoltjer.com vagrant box"
echo "-- pushd thomaswoltjer.com"
pushd thomaswoltjer.com
echo "-- vagrant up"
vagrant up
echo "-- popd"
popd

echo "Setting up firstrpc.org vagrant box"
echo "-- pushd firstrpc.org"
pushd firstrpc.org
echo "-- vagrant up"
vagrant up
echo "-- popd"
popd
