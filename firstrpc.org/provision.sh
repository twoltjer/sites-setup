#!/bin/bash

# Exit on error
set -e

# Add gotop to the vm, for monitoring
echo "Provision.sh: installing snap packages"
snap install gotop
snap install nmap

# Update repositories and install packages
echo "Provision.sh: installing from repositories"
export DEBIAN_FRONTEND=noninteractive
apt-get update -q
apt-get install -q -y postgresql apache2 autossh php libapache2-mod-php php-gd php-xml php-pgsql php-mbstring tmux

# Copy web root
echo "Provision.sh: copying web root files"
cp -r /vagrant/files/webroot /var/www/firstrpc.org

# Stop apache and link web root
echo "Provision.sh: stopping web server"
systemctl stop apache2
a2dissite 000-default

# Enable php modules
echo "Provision.sh: enabling php modules"
phpenmod dom
phpenmod gd
phpenmod pgsql
phpenmod mbstring

# Make site files directory
echo "Provision.sh: setting site files permissions"
chmod 755 -R /var/www/firstrpc.org
chmod 777 -R /var/www/firstrpc.org/sites/default/files/

# Git config
echo "Provision.sh: configuring git"
git config --global user.email "thomas@thomaswoltjer.com"
git config --global user.name "FirstRpc.org web server"
echo -e "Host gitlab.com\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config

# Copy sudoers include file (automated backup requires passwordless sudo for db access)
echo "Provision.sh: copying sudoers"
cp /vagrant/files/configuration/firstrpc.org.sudoers /etc/sudoers.d/

# Create script for backuping up a postgres database to a file
echo "Provision: installing db_backup"
cp /vagrant/files/scripts/db_backup /usr/local/bin/
chmod 755 /usr/local/bin/db_backup

# Copy backup script
echo "Provision.sh: installing web_backup"
cp /vagrant/files/scripts/firstrpc.org.backup.sh /usr/local/bin/web_backup
chmod 755 /usr/local/bin/web_backup

# Configure bash
echo "Provision.sh: configuring bash aliases"
cp /vagrant/files/scripts/bash_aliases /home/vagrant/.bash_aliases

# Load database from file
echo "Provision.sh: restoring database from file"
su postgres -c 'psql < /vagrant/files/db/firstrpc.org.db'

# Enable and start apache server
echo "Provision.sh: configuring apache"
cp /vagrant/files/configuration/firstrpc.org.conf /etc/apache2/sites-available
a2ensite firstrpc.org
a2enmod rewrite
systemctl enable apache2
systemctl start apache2
