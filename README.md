# Sites-Setup

Setup files for web servers. The actual server files are in individual repositories (for security reasons), so an 
authorized RSA key will be required for setup to complete successfully.

Each web server has its own vagrant box. To set up, either enter each subdirectory and run `vagrant up` or run the 
`setup.sh` script, which checks for dependencies and automatically sets up servers.

Note that this setup process may download several large files/repositories, so a fast and unmetered internet connection 
is strongly recommended. 

Questions and requests for access should be addressed to thomas (at) thomaswoltjer.com.